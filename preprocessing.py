from sklearn.model_selection import train_test_split
import json
import os
import pandas as pd
from numpy.random import uniform
from collections import Counter
from atomic import Atomic2020
from tqdm import tqdm
from sentence_transformers import SentenceTransformer

RANDOM_STATE = 201942

def generate_neutral(data):
    data['context'] = data['id'].str.split('_', expand=True)[0]

    neutral_rels = None
    for (split, context), group in data.groupby(['split', 'context']):
        group = pd.concat([group, pd.DataFrame(group['Arg1']).merge(group['Arg2'], how='cross')], axis=0).drop_duplicates().reset_index(drop=True)
        group['rel'] = 'neutral'
        group['split'] = split
        if neutral_rels is None:
            neutral_rels = group
        else:
            neutral_rels = pd.concat([neutral_rels, group], axis=0)

    neutral_rels = neutral_rels.sample(n=data.shape[0]//2, random_state=RANDOM_STATE)
    data = pd.concat([data, neutral_rels], axis=0)

    return data
# Write atomic triples to files
def compute_relations(dataset='student_essay'):
    data = None
    root = f'data/paul_et_al/{dataset}'
    rel_dict = {'attacks':0, 'supports':1, 'attack':0, 'support':1, 'neutral':2}
    for file in os.listdir(root):
        if '.txt' in file:
            # print(file)
            df = pd.read_csv(os.path.join(root, file),
            names=['id', 'Arg1', 'x', 'Arg2', 'y', 'conceptnet', 'rel'],
            usecols=['id', 'Arg1', 'Arg2', 'conceptnet', 'rel'],
            index_col=None,
            delimiter='\t')
            df['split'] = file.split('_')[0]
            if data is None:
                data = df
            else:
                data = pd.concat([data, df])

    data = generate_neutral(data)
    data['rel'] = data['rel'].apply(lambda x: rel_dict[x])



    kg2020 = Atomic2020()
    knowledge_2020 = []
    knowledge_2020_conf = []

    for i, row in tqdm(data.iterrows(), total=data.shape[0]):
        a = row['Arg1']
        b = row['Arg2']
        # k, conf = kg.process_args(a, b)
        # knowledge.append(k)
        # knowledge_conf.append(conf)
        k, conf = kg2020.process_args(a, b)
        knowledge_2020.append(k)
        knowledge_2020_conf.append(conf)

    data['atomic2020'] = knowledge_2020
    data['atomic2020_conf'] = knowledge_2020_conf

    return data

if __name__ == '__main__':
    # prep()
    compute_relations('student_essay').to_csv('data/student_essay.csv')
    compute_relations('debate').to_csv('data/debate.csv')
