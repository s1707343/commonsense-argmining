This repo contains the code for our EACL 2023 paper: [Uncovering Implicit Inferences for Improved Relational Argument Mining](https://aclanthology.org/2023.eacl-main.182/). In this work we generate commonsense inference paths to connect argumentative units together, these paths are then used in a knowledge enhanced classifier to determine the argumentative relationship between the two units.

# Generating Commonsense Paths
TBD

# Training and Evaluation
To recreate our results in table 5:
1. Install requirements with 'conda env create -f environment.yml'
2. Activate the environment: 'conda activate amkg'
3. Add the data csv files to the data directory
4. Train a model with the required configuration:
`python train.py -datadir ./data/{dataset name} -knowledge {knowledge source}  -save {save directory} -model attn -bsz 10 -lr 0.00001 -epochs 10 -base_model roberta-base`

For datasets you may choose 'student_essay.csv', 'debate.csv' or 'presidential.csv', note that .
Knowledge sources are: none, conceptnet, atomic2020, comet and comet_link
Model architectures: hybrid (siamese model without attention), attn (siamese model with attention) and crossencoder
Crossencoder is only used when knowledge source is 'none'.

To view the results you can use tensorboard with the command:
`tensorboard --logdir={save directory}`
