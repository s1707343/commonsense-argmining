from argparse import ArgumentParser

import torch
import argparse


def pargs():
  parser = argparse.ArgumentParser(description='')

  #model
  parser.add_argument("-base_model",default="bert-base-uncased",help="huggingface model to use." )
  parser.add_argument("-esz",default=300,type=int,help="embedding size")
  parser.add_argument("-glove",action="store_true",help="use glove embeddings")
  parser.add_argument("-hsz",default=300,type=int,help="hidden state size")
  parser.add_argument("-drop",default=0.1,type=float,help="dropout rate")
  parser.add_argument("-ckpt", type=str,help="load from checkpoint")
  parser.add_argument("-model", default='crossencoder', help='choose model')
  parser.add_argument("-knowledge", default='none', type=str, help='knowledge source')
  # training and loss
  parser.add_argument("-bsz",default=64,type=int)
  parser.add_argument("-epochs",default=20,type=int)
  parser.add_argument("-clip",default=1,type=float,help='clip grads')
  parser.add_argument("-link_model",default='link', type=str)

  parser.add_argument("-loss",default="cross_entropy",type=str)


  parser.add_argument("-lr",default=0.1,type=float,help='learning rate')
  parser.add_argument("-lrhigh",default=0.5,type=float,help="high learning rate for cycling")
  parser.add_argument("-lrstep",default=4, type=int,help='steps in cycle')
  parser.add_argument("-lrwarm",action="store_true",help='use cycling learning rate')
  parser.add_argument("-lrdecay",default=0.1,type=float,help="use learning rate decay")



  #data
  parser.add_argument("-nosave",action='store_false',help='dont save')
  parser.add_argument("-save",required=True,help="where to save model")
  parser.add_argument("-datadir",default="./data/")
  parser.add_argument("-data",default="preprocessed.train.tsv",help="preprocessed data")
  parser.add_argument("-traindata",default="preprocessed.train.tsv",help="preprocessed train data")
  parser.add_argument("-savevocab",default=None,type=str)
  parser.add_argument("-loadvocab",default=None,type=str)
  parser.add_argument("-n_classes", default=2, type=int)

  #eval
  parser.add_argument("-eval",action='store_true')


  #inference
  parser.add_argument("-test",action='store_true')

  parser.add_argument("-gpu",default=0,type=int)
  args = parser.parse_args()
  if args.gpu == -1:
    args.gpu = 'cpu'
  args.device = torch.device(args.gpu)

  return args
