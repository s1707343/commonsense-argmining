import json
import torch
import argparse
from tqdm import tqdm
from pathlib import Path
from transformers import AutoModelForSeq2SeqLM, AutoTokenizer
from utils import use_task_specific_params, trim_batch

from myparser import pargs
import pickle
import networkx as nx
from scipy.spatial.distance import cosine
from tqdm import tqdm

import sys
import os

import pandas as pd

def chunks(lst, n):
    """Yield successive n-sized chunks from lst."""
    for i in range(0, len(lst), n):
        yield lst[i : i + n]


class Comet:
    def __init__(self, model_path, progress_bar=True):
        self.device = "cuda" if torch.cuda.is_available() else "cpu"
        self.model = AutoModelForSeq2SeqLM.from_pretrained(model_path).to(self.device)
        self.tokenizer = AutoTokenizer.from_pretrained(model_path)
        task = "summarization"
        use_task_specific_params(self.model, task)
        self.batch_size = 1
        self.decoder_start_token_id = None
        self.disable_progress = not(progress_bar)

    def generate(
            self,
            queries,
            decode_method="beam",
            num_generate=5,
            ):

        with torch.no_grad():
            examples = queries

            decs = []
            for batch in tqdm(list(chunks(examples, self.batch_size)), disable=self.disable_progress):

                batch = self.tokenizer(batch, return_tensors="pt", truncation=True, padding="max_length").to(self.device)
                input_ids, attention_mask = trim_batch(**batch, pad_token_id=self.tokenizer.pad_token_id)

                summaries = self.model.generate(
                    input_ids=input_ids,
                    attention_mask=attention_mask,
                    decoder_start_token_id=self.decoder_start_token_id,
                    num_beams=num_generate,
                    num_return_sequences=num_generate,
                    )

                dec = self.tokenizer.batch_decode(summaries, skip_special_tokens=True, clean_up_tokenization_spaces=False)
                decs.append(dec)

            return decs


def build_tree(head, rels, model, max_depth=2):
    G = nx.DiGraph()
    G.add_node(head)
    dists = {}
    queries = [["{} {} [GEN]".format(head, rel) for rel in rels]]
    head = [head]
    accepted = []
    for _ in range(max_depth):
        for i, query in enumerate(queries):
            results = model.generate(query, decode_method="beam", num_generate=1)
            for r, result in enumerate(results):
                for node in result:
                    if node !=' none':
                        G.add_edge(head[i], node, label=rels[r])
                        accepted.append(node)

        head = accepted
        queries = [["{} {} [GEN]".format(node, rel) for rel in rels] for node in head]

    G.remove_edges_from(nx.selfloop_edges(G))
    return G


if __name__ == "__main__":
    args = pargs()
    dataset = args.datadir
    df = pd.read_csv(f'data/{dataset}.csv')
    print("loading comet ...")
    comet = Comet("./comet-atomic_2020_BART", progress_bar=False)
    comet.model.zero_grad()
    print("model loaded")



    # tree = build_tree(root, ['Causes', 'isBefore', 'isAfter', 'HinderedBy'], max_depth=3)
    with open(f'trees_{dataset}.p', 'wb') as f:
        heads = pd.unique(df[['Arg1', 'Arg2']].values.ravel('K'))
        for arg in tqdm(heads, total=len(heads)):
            G = build_tree(arg, ['xNeed', 'xWant', 'xEffect', 'xAttr'], comet, max_depth=3)
            pickle.dump((arg, G), f)
