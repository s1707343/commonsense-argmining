from transformers import AutoModel, AutoModelForSequenceClassification

import torch
from torch.optim.lr_scheduler import LinearLR
from torch.optim import AdamW

import torch.nn as nn
import torch.nn.functional as F
from sklearn.metrics import precision_recall_fscore_support, accuracy_score

import pytorch_lightning as pl
from pandas import DataFrame

def mean_pooling(token_embeddings, attn_mask):
    mean_pooled = token_embeddings.sum(axis=1) / attn_mask.sum(axis=-1).unsqueeze(-1)
    return mean_pooled

class BaseTransformer(pl.LightningModule):
    def __init__(self, lr, loss, decay=0.1):
        super(BaseTransformer, self).__init__()
        self.lr = lr
        self.loss = loss
        self.decay = decay

    def configure_optimizers(self):
        optimizer = AdamW(self.parameters(), lr=self.lr, weight_decay=self.decay)
        lr_scheduler = LinearLR(optimizer)
        return [optimizer], [lr_scheduler]

    def validation_epoch_end(self, validation_step_outputs):
        preds = torch.cat([x[0] for x in validation_step_outputs]).detach().cpu().numpy()
        labels = torch.cat([x[1] for x in validation_step_outputs]).detach().cpu().numpy()
        acc = accuracy_score(labels, preds)
        p, r, f1, _ = precision_recall_fscore_support(labels, preds, average='macro')

        metrics = {'accuracy': acc, 'precision': p, 'recall': r, 'f1':f1}
        for k, v in metrics.items():
            self.log(f"val_{k}", v)
        return metrics

    def test_epoch_end(self, test_step_outputs):
        preds = torch.cat([x[0] for x in test_step_outputs]).detach().cpu().numpy()
        labels = torch.cat([x[1] for x in test_step_outputs]).detach().cpu().numpy()
        acc = accuracy_score(labels, preds)
        p, r, f1, _ = precision_recall_fscore_support(labels, preds, average='macro')

        metrics = {'accuracy': acc, 'precision': p, 'recall': r, 'f1':f1}
        for k, v in metrics.items():
            self.log(f"test_{k}", v)
        return metrics

    def predict_epoch_end(self, predict_step_outputs):
        preds = torch.cat([x[0] for x in predict_step_outputs])
        labels = torch.cat([x[1] for x in predict_step_outputs])
        return preds, labels




class TransformerInjection(BaseTransformer):
    def __init__(self, vocab, lr=0.0001, decay=0.1, model='bert-base-uncased', encoder_dim=768, hsize=128, n_labels=3):
        super(TransformerInjection, self).__init__(lr=lr, loss=F.cross_entropy, decay=decay)

        self.transformer = AutoModel.from_pretrained(model)
        self.transformer.resize_token_embeddings(vocab)

        self.pooling = mean_pooling

        self.linear =  nn.Linear(encoder_dim*2, hsize)
        self.classifier = nn.Linear(hsize, n_labels)

        self.lr = lr

    def forward(self, texts):
        x, k = texts

        token_embeddings = self.transformer(**x)['last_hidden_state']
        sentence_embedding = mean_pooling(token_embeddings, x['attention_mask'])

        token_embeddings =self.transformer(**k)['last_hidden_state']
        kg_embedding = mean_pooling(token_embeddings, k['attention_mask'])

        sentence_embedding = torch.cat([sentence_embedding, kg_embedding], axis=-1)
        sentence_embedding = self.linear(sentence_embedding)
        sentence_embedding = torch.tanh(sentence_embedding)
        logits = self.classifier(sentence_embedding)

        return logits

    def training_step(self, batch, batch_idx):
        batch, ks = batch
        labels = batch.pop('labels', None)

        outputs = self((batch, ks))
        loss = self.loss(outputs, labels)
        self.log("train_loss", loss, on_step=True, on_epoch=True, prog_bar=True, logger=True)

        return loss

    def validation_step(self, batch, batch_idx):
        batch, ks = batch
        labels = batch.pop('labels', None)

        outputs = self((batch, ks))
        loss = self.loss(outputs, labels)
        self.log("val_loss", loss)

        preds = outputs.argmax(axis=-1)
        return preds, labels

    def test_step(self, batch, batch_idx):
        batch, ks = batch
        labels = batch.pop('labels', None)

        outputs = self((batch, ks))
        loss = self.loss(outputs, labels).item()
        self.log("test_loss", loss)
        preds = outputs.argmax(axis=-1)
        return preds, labels

    def predict_step(self, batch, batch_idx):
        batch, ks = batch
        labels = batch.pop('labels', None)

        outputs = self((batch, ks))
        loss = self.loss(outputs, labels).item()
        preds = outputs.argmax(axis=-1)
        return preds, labels

class TransformerKnowledgeAttention(BaseTransformer):
    def __init__(self, vocab, lr=0.0001, decay=0.1, model='distilbert-base-uncased', encoder_dim=768, hsize=128, n_labels=3):
        super(TransformerKnowledgeAttention, self).__init__(lr=lr, loss=F.cross_entropy, decay=decay)

        self.transformer = AutoModel.from_pretrained(model)
        self.transformer.resize_token_embeddings(vocab)

        self.pooling = mean_pooling

        self.attention = nn.MultiheadAttention(encoder_dim, 4, batch_first=True)



        self.linear =  nn.Linear(encoder_dim*2, hsize)
        self.classifier = nn.Linear(hsize, n_labels)

        self.lr = lr

    def forward(self, texts):
        x, k = texts

        token_embeddings = self.transformer(**x)['last_hidden_state']
        sentence_embedding = mean_pooling(token_embeddings, x['attention_mask'])
        knowledge_embeddings = self.transformer(**k)['last_hidden_state']

        enhanced_knowledge_embeddings = torch.tanh(self.attention(token_embeddings, knowledge_embeddings, knowledge_embeddings)[0])
        # sentence_embedding = mean_pooling(token_embeddings, x['attention_mask'])

        # token_embeddings =self.transformer(**k)['last_hidden_state']
        enhanced_knowledge_embeddings = mean_pooling(enhanced_knowledge_embeddings, k['attention_mask'])

        sentence_embedding = torch.cat([sentence_embedding, enhanced_knowledge_embeddings], axis=-1)
        sentence_embedding = self.linear(sentence_embedding)
        sentence_embedding = torch.tanh(sentence_embedding)
        logits = self.classifier(sentence_embedding)

        return logits

    def training_step(self, batch, batch_idx):
        batch, ks = batch
        labels = batch.pop('labels', None)

        outputs = self((batch, ks))
        loss = self.loss(outputs, labels)
        self.log("train_loss", loss, on_step=True, on_epoch=True, prog_bar=True, logger=True)

        return loss

    def validation_step(self, batch, batch_idx):
        batch, ks = batch
        labels = batch.pop('labels', None)

        outputs = self((batch, ks))
        loss = self.loss(outputs, labels)
        self.log("val_loss", loss)

        preds = outputs.argmax(axis=-1)
        return preds, labels

    def test_step(self, batch, batch_idx):
        batch, ks = batch
        labels = batch.pop('labels', None)

        outputs = self((batch, ks))
        loss = self.loss(outputs, labels).item()
        self.log("test_loss", loss)
        preds = outputs.argmax(axis=-1)
        return preds, labels

    def predict_step(self, batch, batch_idx):
        batch, ks = batch
        labels = batch.pop('labels', None)

        outputs = self((batch, ks))
        loss = self.loss(outputs, labels).item()
        preds = outputs.argmax(axis=-1)
        return preds, labels



class TransformerCrossEncoder(BaseTransformer):
    def __init__(self, vocab, lr=0.0001, decay=0.1, model='bert-base-uncased', n_labels=3):
        super(TransformerCrossEncoder, self).__init__(lr=lr, loss=F.cross_entropy, decay=decay)

        self.transformer = AutoModelForSequenceClassification.from_pretrained(model, num_labels=n_labels)
        self.transformer.resize_token_embeddings(vocab)

    def forward(self, x):
        logits = self.transformer(**x, return_dict=False)[0]
        return logits

    def training_step(self, batch, batch_idx):
        labels = batch.pop('labels', None)

        outputs = self(batch)
        loss = self.loss(outputs, labels)
        self.log("train_loss", loss, on_step=True, on_epoch=True, prog_bar=True, logger=True)

        return loss

    def validation_step(self, batch, batch_idx):
        labels = batch.pop('labels', None)

        outputs = self(batch)
        loss = self.loss(outputs, labels)
        self.log("val_loss", loss)

        preds = outputs.argmax(axis=-1)
        return preds, labels

    def test_step(self, batch, batch_idx):
        labels = batch.pop('labels', None)

        outputs = self(batch)
        loss = self.loss(outputs, labels).item()
        self.log("test_loss", loss)
        preds = outputs.argmax(axis=-1)
        return preds, labels

    def predict_step(self, batch, batch_idx):
        labels = batch.pop('labels', None)

        outputs = self(batch)
        loss = self.loss(outputs, labels).item()
        preds = outputs.argmax(axis=-1)
        return preds, labels

class LinkPred(BaseTransformer):
    def __init__(self, lr=0.0001, decay=0.1, model='bert-base-uncased', n_labels=5):
        super(LinkPred, self).__init__(lr=lr, loss=F.cross_entropy, decay=decay)

        self.transformer = AutoModelForSequenceClassification.from_pretrained(model, num_labels=n_labels)

    def forward(self, x):
        logits = self.transformer(**x, return_dict=False)[0]
        return logits

    def training_step(self, batch, batch_idx):
        labels = batch.pop('labels', None)

        outputs = self(batch)
        loss = self.loss(outputs, labels)
        self.log("train_loss", loss, on_step=True, on_epoch=True, prog_bar=True, logger=True)

        return loss

    def validation_step(self, batch, batch_idx):
        labels = batch.pop('labels', None)

        outputs = self(batch)
        loss = self.loss(outputs, labels)
        self.log("val_loss", loss)

        preds = outputs.argmax(axis=-1)
        return preds, labels

    def test_step(self, batch, batch_idx):
        labels = batch.pop('labels', None)

        outputs = self(batch)
        loss = self.loss(outputs, labels).item()
        self.log("test_loss", loss)
        preds = outputs.argmax(axis=-1)
        return preds, labels

    def predict_step(self, batch, batch_idx):
        labels = batch.pop('labels', None)

        outputs = self(batch)
        loss = self.loss(outputs, labels).item()
        preds = outputs.argmax(axis=-1)
        return preds, labels


def default_collator(batch):
    txt = {}
    txt_keys = batch[0].keys()
    for k in txt_keys:
        txt[k] = torch.stack([t[k] for t in batch])

    return txt
