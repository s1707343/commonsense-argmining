from sklearn.metrics import f1_score, accuracy_score
import numpy as np
import torch
import torch.nn.functional as F

def load_embedding(esz):
    vocab = {'<pad>':0, '<unk>':1}
    embeddings = []
    with open(f'embeddings/glove.6B/glove.6B.{esz}d.txt','rt') as fi:
        full_content = fi.read().strip().split('\n')
    for i in range(len(full_content)):
        i_word = full_content[i].split(' ')[0]
        i_embeddings = [float(val) for val in full_content[i].split(' ')[1:]]
        vocab[i_word] = i + 2

        embeddings.append(i_embeddings)

    emb_np = np.array(embeddings)
    pad_emb_np = np.zeros((1,emb_np.shape[1]))   #embedding for '<pad>' token.
    unk_emb_np = np.mean(emb_np,axis=0,keepdims=True)    #embedding for '<unk>' token.

    #insert embeddings for pad and unk tokens at top of embs_npa.
    emb_np = np.vstack((pad_emb_np,unk_emb_np,emb_np))


    return vocab, emb_np

def print_metrics(true, pred):
    true = torch.cat(true, dim=0).detach().cpu().numpy()
    pred = torch.cat(pred, dim=0).detach().cpu().numpy()

    return f1_score(true, pred, average='macro'), accuracy_score(true, pred)

def f1_loss(pred, true):
    o = F.softmax(pred, dim=-1)
    t = F.one_hot(true, 3)

    tp = torch.sum(o*t)
    tn = torch.sum((1-o)*(1-t))
    fp = torch.sum(o*(1-t))
    fn = torch.sum((1-o)*t)

    e = 1e-7

    p = tp / (tp + fp + e)
    r = tp / (tp + fn + e)

    f1 = 2*(p*r)/(p + r + e)

    return 1-f1

def use_task_specific_params(model, task):
    """Update config with summarization specific params."""
    task_specific_params = model.config.task_specific_params

    if task_specific_params is not None:
        pars = task_specific_params.get(task, {})
        model.config.update(pars)

def trim_batch(
    input_ids, pad_token_id, attention_mask=None,
):
    """Remove columns that are populated exclusively by pad_token_id"""
    keep_column_mask = input_ids.ne(pad_token_id).any(dim=0)
    if attention_mask is None:
        return input_ids[:, keep_column_mask]
    else:
        return (input_ids[:, keep_column_mask], attention_mask[:, keep_column_mask])
